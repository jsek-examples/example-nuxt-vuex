# Demo for Nuxt + Vuex

> Nuxt starter with Vuex

---

![](./screenshot.png)

## Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
