import Vuex from "vuex";

export const mutations = {
    reset(state, payload) { state.counter = payload || 0; },
    increment(state, payload) { if (payload > 0) state.counter += payload; },
    decrement(state, payload) { if (payload > 0) state.counter -= payload; }
}

export const actions = {
    reload({commit, state}) {
        return fetch("https://www.random.org/integers/?num=1&min=1&max=2000&col=1&base=10&format=plain&rnd=new", {
                credentials: "same-origin",
                mode: "cors"
            })
            .then(response => response.json())
            .then(x => {
                if (state.counter >= 0) { commit('reset', - (x % 100)); }
                else if (state.counter < 0) { commit('reset', x % 100); }
            });
    }
}

export default () =>
    new Vuex.Store({
        state: () => ({ counter: 10 }),
        mutations,
        actions
    });