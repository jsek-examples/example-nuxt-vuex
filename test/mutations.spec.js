import { mutations } from '../store';

describe('Vuex:mutations:[Counter]', () => {

    describe('increment', () => {
        it('should work with given step', () => {
            const state = {
                counter: 0
            };

            mutations.increment(state, 1);
            expect(state.counter).toBe(1);

            mutations.increment(state, 6);
            expect(state.counter).toBe(7);

            state.counter = -11;
            mutations.increment(state, 4);
            expect(state.counter).toBe(-7);
        })

        it('should work only with positive argument', () => {
            const state = {
                counter: 5
            };

            mutations.increment(state, 11);
            expect(state.counter).toBe(16);

            mutations.increment(state, -1);
            expect(state.counter).toBe(16);

            mutations.increment(state, 0);
            expect(state.counter).toBe(16);
        })
    })

    describe('decrement', () => {
        it('should work with given step', () => {
            const state = {
                counter: 0
            };

            mutations.decrement(state, 1);
            expect(state.counter).toBe(-1);

            mutations.decrement(state, 7);
            expect(state.counter).toBe(-8);

            state.counter = 21;
            mutations.decrement(state, 34);
            expect(state.counter).toBe(-13);
        })

        it('should work only with positive argument', () => {
            const state = {
                counter: 5
            };

            mutations.decrement(state, 11);
            expect(state.counter).toBe(-6);

            mutations.decrement(state, -1);
            expect(state.counter).toBe(-6);

            mutations.decrement(state, 0);
            expect(state.counter).toBe(-6);
        })
    })
})