import { actions } from '../store';
import { mutations } from '../store';

describe('Vuex:actions:[Counter]', () => {

    let commits = [];
    const fakeState = { counter: 0 };
    const fakeContext = {
        commit: (event, args) => {
            commits.push({event, args})
            mutations[event](fakeState, args)
        },
        state: fakeState
    };

    describe('reload', () => {

        beforeEach(() => {
            commits = [];
            fetch.resetMocks();
        })
        
        it('should set new value once', async () => {
            fetch.once('5');
            await actions.reload(fakeContext);
            const resetCount = commits.filter(x => x.event === 'reset').length;
            expect(resetCount).toEqual(1);
        })
        
        it('should match random value', async () => {
            fetch.once('5');
            fakeState.counter = -1;
            await actions.reload(fakeContext);
            expect(commits).toContainEqual({event: 'reset', args: 5});
        })

        it('should normalize fetched value if it exeeded range', async () => {
            fetch.once('225');
            fakeState.counter = -10;
            await actions.reload(fakeContext);
            expect(commits).toContainEqual({event: 'reset', args: 25});
        })

        it('should get positive value if counter is negative', async () => {
            fetch.once('66');
            fakeState.counter = -10;
            await actions.reload(fakeContext);
            expect(commits).toContainEqual({event: 'reset', args: 66});
        })

        it('should get negative value if counter is positive', async () => {
            fetch.once('15');
            fakeState.counter = 10;
            await actions.reload(fakeContext);
            expect(commits).toContainEqual({event: 'reset', args: -15});
        })
    })
})