const pkg = require("./package");

module.exports = {
  mode: "universal",

  head: {
    title: pkg.name,
    meta: [{
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: pkg.description
      }
    ],
    link: [{
      rel: "icon",
      type: "image/x-icon",
      href: "/favicon.ico"
    },{
      rel: "styleshee",
      href: "https://fonts.googleapis.com/css?family=Krub"
    }]
  },

  loading: {
    color: "#3b8070"
  },

  css: [
    'vuetify/dist/vuetify.min.css'
  ],

  plugins: [
    '~plugins/vuetify.js'
  ],

  modules: [
    "@nuxtjs/axios",
  ],
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  build: {
    extend(config, ctx) {}
  }
};
